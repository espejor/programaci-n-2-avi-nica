#ifndef MENU_H
#define MENU_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
// #include <WProgram.h>
#endif
#define LONGITEM 13

#include <string.h>
class ItemMenu;

class Menu {
public:
	Menu(int numItems, char *listaItems[], ItemMenu *ptrItem);
	bool isVisible();   //Devuelve si un menú está visible
	void showMenu();   //Presenta el menú en pantalla
	int sigItem();      //Pasa y Devuelve el orden del siguiente item del menú
	int antItem();      //Pasa y Devuelve el orden del anterior item del menú
	void selectItem();  //Ejecuta la función asociada al item seleccinado
	int actCursor();    //Devuelve el orden del item actual
	char* txtSelected();//Devuelve el texto del cursor actual
	void verItem(int numItem);
	//    void putHijo(int opcion,Menu* newHijo);
	ItemMenu* getItem(int orden);
	//    void agregaItem (ItemMenu *item);
private:
	ItemMenu *items;  // Array de  Objetos Items que componen el menú
	int numItems;      // Número de items del menú (tamańo del array)
	int ultVisible;    // último item visible
	int cursor;        // Item seleccionado actualmente
	bool visible;    // Si el menú está visible
	ItemMenu* itemPadre;
};
class ItemMenu{
private:
	Menu *menuPadre;
	Menu *menuHijo;
	char *item;
	int orden;
public:
	ItemMenu(char _item[], int _orden, Menu *padre); // Constructor
	ItemMenu();
	char* txtItem();    //Devuelve un puntero al texto del item
	int getOrden();     //Devuelve el orden del item en el menú
	ItemMenu operator = (ItemMenu);
	Menu* tieneHijos();
	void putHijo(Menu* newHijo);
	~ItemMenu();
};


#endif // MENU_H
