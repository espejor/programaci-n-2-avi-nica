//#include <iostream>
#include <stdio.h>
#include "Menu.h"

//using namespace std;

int main()
{
	char *listaInicio[] = { "Primero", "Segundo", "Tercero", "Cuarto" };
	char *listaPrimero[] = { "PA", "PB", "PC" };
	char *listaTercero[] = { "TA", "TB" };
	char *listaCuarto[] = { "CA", "CB", "CC", "CD" };

	Menu inicial(sizeof(listaInicio) / sizeof(listaInicio[0]), listaInicio, NULL);
	Menu primero(sizeof(listaPrimero) / sizeof(listaPrimero[0]), listaPrimero, inicial.getItem(0));
	Menu tercero(sizeof(listaTercero) / sizeof(listaTercero[0]), listaTercero, inicial.getItem(2));
	Menu cuarto(sizeof(listaCuarto) / sizeof(listaCuarto[0]), listaCuarto, inicial.getItem(3));

	inicial.showMenu();

	inicial.sigItem();
	inicial.showMenu();
	inicial.sigItem();
	inicial.showMenu();
	inicial.sigItem();
	inicial.showMenu();
	inicial.antItem();
	inicial.showMenu();
	inicial.antItem();
	inicial.showMenu();
	inicial.antItem();
	inicial.showMenu();
	inicial.antItem();
	inicial.showMenu();

	return 0;
}
