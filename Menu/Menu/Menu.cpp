#include "Menu.h"
#include <stdio.h>
//#define ITEM 13

Menu::Menu(int numItems, char *listaItems[], ItemMenu  *ptrPadre){
	cursor = 0;                     // Inicializamos el cursor a 0
	visible = false;                // Inicializamos visible a falso
	this->numItems = numItems;      // Inicializamos el número de items
	items = new ItemMenu[numItems];
	itemPadre = ptrPadre;
	if (itemPadre) itemPadre->putHijo(this);
	for (int i = 0; i<numItems; i++){   // Rellenamos el array de punteros a Items
		ItemMenu itemTemp(listaItems[i], i, this);
		items[i] = itemTemp;
	}
}



int Menu::actCursor(){
	return cursor;
}

bool Menu::isVisible(){
	return visible;
}

int Menu::sigItem(){
	if (cursor < numItems - 1) cursor++;
	return cursor;
}

int Menu::antItem(){
	if (cursor > 0) cursor--;
	return cursor;
}

char* Menu::txtSelected(){
	return items[cursor].txtItem();
}

void Menu::verItem(int numItem){
	char texto[LONGITEM + 1];
	strcpy_s(texto, items[numItem].txtItem());
	if (cursor == numItem) {
		printf("%s", ">");
	}
	else{
		printf(" ");
	}
	printf("%s\n", texto);
}

// --------------------esta función es provisional-----------------------
void Menu::showMenu(){
	for (int i = 0; i < numItems; i++){
		verItem(i);
		if (Menu* hijos = items[i].tieneHijos()){
			hijos->showMenu();
		}
	}
}

ItemMenu* Menu::getItem(int orden){
	ItemMenu* ptrItem;
	ptrItem = &(this->items[orden]);
	return ptrItem;
}


//void Menu::putHijo(int opcion, Menu* newHijo){
//    items[opcion].putHijo(newHijo);
//}

//********************** Métodos ItemMenu ***********************

ItemMenu::ItemMenu(){
	item = new char[LONGITEM + 1];
	//    item = 0;
	orden = 0;
	menuPadre = NULL;          // El pintero de menuPadre apunta al Menú contenedor
}
ItemMenu::ItemMenu(char _item[], int _orden, Menu *padre){
	item = new char[LONGITEM + 1];    //Creamos el objeto item como cadena de caracteres
	item = _item;
	orden = _orden;
	menuPadre = padre;          // El puntero de menuPadre apunta al Menú contenedor
	//    menuPadre->putHijo(orden,menuPadre);
}

ItemMenu::~ItemMenu(){
//delete item;
}

char* ItemMenu::txtItem(){
	return this->item;
}

Menu* ItemMenu::tieneHijos(){
	return menuHijo;
}

int ItemMenu::getOrden(){
	return orden;
}

void ItemMenu::putHijo(Menu* newHijo){
	menuHijo = newHijo;
}

ItemMenu ItemMenu::operator = (ItemMenu itemB){
	item = itemB.item;
	orden = itemB.orden;
	menuHijo = itemB.menuHijo;
	menuPadre = itemB.menuPadre;
	return *this;
}
